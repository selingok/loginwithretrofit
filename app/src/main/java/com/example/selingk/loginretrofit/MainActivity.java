package com.example.selingk.loginretrofit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private Button loginButton;
    private Button registerButton;
    private EditText usernameTV;
    private EditText passwordTV;
    UserModel user = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton = (Button) findViewById(R.id.loginButton);
        registerButton = (Button) findViewById(R.id.registerButton);
        usernameTV = (EditText) findViewById(R.id.usernameEditText);
        passwordTV = (EditText) findViewById(R.id.passwordEditText);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.0.19:8080/SpringExample/rest/user")
                .build();


        final UserAPI service = restAdapter.create(UserAPI.class);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user=new UserModel();
                user.setUsername(usernameTV.getText().toString());
                user.setPassword(passwordTV.getText().toString());

                service.login(user, new Callback<ResponseModel>() {
                    @Override
                    public void success(ResponseModel responseModel, Response response) {
                        if(responseModel.getStatus() == 200){
                            usernameTV.setText("");
                            passwordTV.setText("");
                            Intent intend = new Intent("com.example.selingk.loginretrofit.User");
                            startActivity(intend);
                            Message(responseModel.getMessage());
                        ;}
                        else
                            Message(responseModel.getMessage());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Message(error.getMessage());
                    }
                });

            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user = new UserModel();
                user.setUsername(usernameTV.getText().toString());
                user.setPassword(passwordTV.getText().toString());
                service.addUser(user, new Callback<ResponseModel>() {
                    @Override
                    public void success(ResponseModel responseModel, Response response) {
                        Message(responseModel.getMessage());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Message(error.getMessage());
                    }
                });

                usernameTV.setText("");
                passwordTV.setText("");
            }
        });

        }

    private void Message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    }

